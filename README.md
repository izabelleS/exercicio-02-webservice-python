# Documentação
# WebService em Python com Flask Framework 

## Clonando o Repositório 'exercicio-02-webservice-python'
> git clone https://gitlab.com/pixi3s/exercicio-02-webservice-python
```
Será criado um diretório chamado: exercicio-02-webservice-python
Necessário entrar no diretório do seu repositório, use o comando:
```
> cd exercicio-02-webservice-python

----------

## Criando ambiente virtual para o Python
> python3 -m venv meu-venv

```
1. -m = Argumento usado para carregar modulos
2. venv = Nome do Modulo que precisamos usar
3. meu-venv = Nome do meu ambiente virtual python
```
----------
## Iniciando ambiente virtual

### No Windows
> .\meu-venv\Scripts\activate
### No Linux
> . meu-venv/bin/activate

```
Uma vez criado e iniciado todas as ferramentas que você precisar \
para desenvolvimento ou produção será mantido apenas neste ambiente.
```
----------

## Ambiente isolado.
Quando o seu ambiente estiver ativo o seu prompt de comandos te mostrar algo como:
> (meu-venv) 
```
Agora podemos se necessário instalar bibliotecas, frameworks entre outros e assim \
se manter suas bibliotecas organizadas.
```
----------

## Instalando dependências necessárias
As dependências estão gravadas no arquivo requirements.txt, para inslatar use o comando:
> pip install -r requirements.txt
```
Este procedimento fará o download e instalação automatizada das dependências necessárias.
``` 
----------

## Começando a escrever nossa aplicação:
Vamos entender o comportamento da web... CONTINUANDO...

#### O documento anterior foi copiado para o arquivo PROBLEMA.md
