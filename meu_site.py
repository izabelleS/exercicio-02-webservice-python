""" doc """
from flask import Flask

app = Flask(__name__)

@app.route('/')
def pagina_principal():
    return "Página Inicial"

@app.route('/sobre')
def pagina_sobre():
    return "Esta é a página que fala sobre a ideia da propria página"

@app.route('/soma/<int:id1>/<int:id2>')
def soma(id1,id2):
    """
    doc
    """
    operacao = id1 + id2
    return str(operacao)

if __name__ == '__main__':
    app.run(debug=True)
